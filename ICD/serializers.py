from rest_framework import serializers
from .models import ICD


class ICDSerializer(serializers.ModelSerializer):
    class Meta:
        model = ICD
        fields = ('baso', 'eos', 'mono', 'neu', 'lym', 'wbc', 'hct', 'hgb', 'rbc', 'mch', 'mchc', 'mcv', 'mpv', 'rdw', 'pdw', 'plt', 'tpttbm', 'pct')