# Import libraries
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .serializers import ICDSerializer
import pandas as pd
from pandas import json_normalize
from sklearn.preprocessing import LabelEncoder
from sklearn.ensemble import RandomForestClassifier
import os


@api_view(['POST'])
def get_post_icd(request):
    # insert a new record for a icd
    if request.method == 'POST':
        data = {
            'baso': request.data.get('baso'),
            'eos': request.data.get('eos'),
            'mono': request.data.get('mono'),
            'neu': request.data.get('neu'),
            'lym': request.data.get('lym'),
            'wbc': request.data.get('wbc'),
            'hct': request.data.get('hct'),
            'hgb': request.data.get('hgb'),
            'rbc': request.data.get('rbc'),
            'mch': request.data.get('mch'),
            'mchc': request.data.get('mchc'),
            'mcv': request.data.get('mcv'),
            'mpv': request.data.get('mpv'),
            'rdw': request.data.get('rdw'),
            'pdw': request.data.get('pdw'),
            'plt': request.data.get('plt'),
            'tpttbm': request.data.get('tpttbm'),
            'pct': request.data.get('pct'),
        }
        # Convert data to JSON
        serializer = ICDSerializer(data=data)
        # If data valid
        if serializer.is_valid():
            serializer.save()

            # Convert JSON to Dataframe
            data_test = json_normalize(serializer.data)

            # training icd dataset and predict
            result = training_icd(data_test)

            return Response(result, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def training_icd(data_test):
    # Feature names of ICD
    col_icd_names = []
    for i in range(20):
        if i == 19:
            col_icd_names.append('Class')
        else:
            col_icd_names.append(str(i + 1))

    # Read dataset from txt file
    ICD_abs_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), './ICD.txt')
    icd_file = pd.read_csv(ICD_abs_path, sep=',', names=col_icd_names)
    # Convert string to float (Class)
    le = LabelEncoder()
    icd_file['Class'] = le.fit_transform(icd_file['Class'])

    # Define independent variable
    X = icd_file.drop('Class', axis=1).drop('1', axis=1)
    # Define dependent variable
    Y = icd_file.Class

    # Predict label by using RF
    model = RandomForestClassifier(n_estimators=10, random_state=42)
    model.fit(X, Y)
    # Predict's result
    prediction_test = model.predict(data_test)
    # Convert from float to string
    result = le.inverse_transform(prediction_test)

    return result

